# podman-deb
Debian/Ubuntu packages for up-to-date Podman releases. 
Shamelessly taken over from [podman-deb](https://gitlab.com/benridley/podman-deb), complemented with arm64 and armhf architecture builds and enhanced with some magic pipeline juju. Binary packages are hosted public, see below.

## Usage
```bash
curl https://minio.team-r.de/podman/public.key | gpg --dearmor -o team-r.de.gpg
sudo echo 'deb [signed-by=team-r.de.gpg] https://minio.team-r.de/podman/repo bullseye main' >> /etc/apt/sources.list
sudo apt update
sudo apt install podman
```

## Disclaimer
Use at your own risk! :P
