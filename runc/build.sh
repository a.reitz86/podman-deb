#!/bin/sh
ARCH=$(dpkg --print-architecture)
PKG_ROOT="$(pwd)/runc_${RUNC_VERSION}-1_${ARCH}"

mkdir -p "${PKG_ROOT}"

git clone https://github.com/opencontainers/runc.git
cd runc
git checkout "v${RUNC_VERSION}"

make BUILDTAGS="seccomp"
make DESTDIR=$PKG_ROOT install
cd ..

mkdir -p "${PKG_ROOT}/DEBIAN"
envsubst < "runc-control" > "${PKG_ROOT}/DEBIAN/control"

dpkg-deb --build ${PKG_ROOT}
